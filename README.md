# Robot Tag: *The Game of Champions*

## Overview
The following is a readme file for the Robot Tag project created as our final assignment in ECE491 at Michigan State University. The scope of this file is to provide the reader with simple instructions on how to run and start the game in a virtual environment and with real robots running ROS. 

### Project Team
- Kevin Smith
- Scott Knaub
- Sam Campbell
- Alisha Jacobs

## The Game
An Autonomous robot game of tag where multiple robots navigate through the arena environment, while trying to avoid obstacles and simultaneously trying to “pursue” or “evade” each other. 

## Rules
- Game Overview: 
	- Turtlebots compete in a game of tag in an 8x8 arena while attempting to avoid four 1x1 obstacles within it.
- Winner:
	- Determined by robot with most accumulated points. 
- Roles: 
	- One robot (pursuer) will try to tag/trap the other robot's (evader's) while they try to avoid being tag/trapped. 
- Tagged: 
	- When the 'pursuer' comes within a calibrated threshold of the 'evader' based on the distance of the pose.
	- Tagged robots remain in the match as stationary obstacles. 
- Scoring Points: 
	- Pursuer: Scores 1 point per robot 'tagged'.
	- Evader: Scores 1 point for avoiding being 'tagged' during the round.
- Rounds: 
	- The game will take place over a 2-minute round per competing robot where each participating robot (determined randomly) gets to start a round as the pursuer.  
	- Rounds will continue until the pursuer robot catches each evader robot or until the duration of the round is met.  
- Referee: 
	- A third bot (laptop) will coordinate the game mode, assign robot roles, and keep score of the game. 
- Setup: 
	- Two to four robots can compete in the game where each robot will start off in opposite corners of the arena.


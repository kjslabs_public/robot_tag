#!/usr/bin/env python
# ==============================================================================
# ECE 491
# Robot Tag - setup.py
# Author: Kevin Smith
# E-mail: smit2958@msu.edu
#
# This file is called by catkin make to ensure the package and the include
# folder is included when the code is executed.
#
# IMPORTANT: Do NOT manually invoke this file. Use the command catkin_make
# instead.
# ==============================================================================

# ===[ IMPORTS ]================================================================
from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

# Fetch values from package.xml
setup_args = generate_distutils_setup(
	packages=['robot_tag'],
	package_dir={'': 'include'},
)

setup(**setup_args)

# ===[ EOF ]====================================================================

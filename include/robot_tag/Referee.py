#!/usr/bin/env python
# ==============================================================================
# ECE 491
# Robot Tag - Referee.py
# Authors: Kevin Smith (smit2958@msu.edu),
#          Scott Knaub (knaubsco@msu.edu),
#          Sam Campbell (campb980@msu.edu),
#          Alisha Jacob (jacobali@msu.edu)
#
# This file contains the Referee class.
# ==============================================================================

# ===[ IMPORTS ]================================================================
import rospy
import roslaunch
import os
import sys
import random
import dircache

# Import Robot tag settings
from robot_tag.CommonSettings import _PLAYERMODES, _VERSION
from robot_tag.RefereeSettings import _REFEREE_NODE_SETTINGS, \
                                      _GAME_SETTINGS, \
                                      _CHARACTER_NAME, \
                                      _PLAYER_STARTING_POSE

# Import Robot tag functions, services, and messsages
from robot_tag.CommonFunctions import _LOGLEVEL as _LVL, \
                                      common_log as log, \
                                      common_checkInputRange, \
                                      common_welcomeMsg, \
                                      common_resultsMsg

from robot_tag.msg import RefereeMsg, PlayerInformation, PlayerMsg, StartingPoseData
from robot_tag.srv import GetPlayerIdentity, GetPlayerIdentityResponse
from robot_tag.srv import PlayerCaptured, PlayerCapturedResponse
from robot_tag.srv import AbortPlayer

# Import sound play messages and library support.
from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient


# ===[ CLASSES ]================================================================
# ------------------------------------------------------------------------------
# Class: playerIdClass
#
# The class defines the functions for the player ID class. This is used to
# track the node ID and which character and player index the node is.
#-------------------------------------------------------------------------------
class playerIdClass():
    def __init__(self):
        self.PlayerId = None
        self.NodeId = None
        self.CharacterIndex = None

    def getPlayerId(self):
        return self.PlayerId

    def setPlayerId(self, data):
        self.PlayerId = data

    def getNodeId(self):
        return self.NodeId

    def setNodeId(self, data):
        self.NodeId = data

    def getCharacterIdx(self):
        return self.CharacterIndex

    def setCharacterIdx(self, data):
        self.CharacterIndex = data


# ------------------------------------------------------------------------------
# Class: Referee
#
# The class defines the functions for the referee node.
#-------------------------------------------------------------------------------
class Referee(object):
    # --------------------------------------------------------------------------
    # Function: __init__
    #
    # This is the constructor of the referee class. The local variables
    # and message information is defined here.
    #---------------------------------------------------------------------------
    def __init__(self):
        # General Class variables
        self.Rate = rospy.Rate(_REFEREE_NODE_SETTINGS['NodeRateHz'])
        self.GameStartControl = False
        self.CurrentPursuer = 0
        self.SelectedNumOfPlayers = 0
        self.CharSelectionList = []

        # Referee Message Data
        self.Refpub = None
        self.Refmsg = RefereeMsg()
        self.RefTimer = None
        self.AbortReq = False

        # Subscriber list for all the players
        self.sub = []
        self.node = []
        self.NavStackCount = 0

        # Service for robots to call
        self.PIsrv = None
        self.PIActive = False

        # Assign sound path
        tmpSoundPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), "../sounds/")
        self.BellSoundPath = os.path.join(tmpSoundPath, 'Start/')
        self.GameOverSoundPath = os.path.join(tmpSoundPath, 'Outro/')

        # Create shutdown hook that is called before the shutdown flag in
        # ROS is called.
        rospy.on_shutdown(self._shutdown_hook)

    # ==========================================================================
    #
    #
    # INIT FUNCTIONS
    #
    #
    # ==========================================================================

    # --------------------------------------------------------------------------
    # Function: init
    #
    # This function shall be called after the class is created to establish
    # the targeted number of players and any other settings before broadcasting
    # to the other nodes.
    #---------------------------------------------------------------------------
    def init(self):
        self._init_WelcomeMessage()
        self._init_getNumberOfPlayers()

        # Now that the number of players is selected, lets enable the service
        # to assign characters.
        self.PIsrv = rospy.Service('/robot_tag/Referee/GetPlayerIdentity', GetPlayerIdentity, self._srv_GetPlayerIdentity)

        # Set up inital message and configPlayerInfoure timer for broadcasting
        self._setup_RefereeMessage()

        # Now the the referee intialization is complete, start the main function
        # of the node.
        self.startReferee()

    # --------------------------------------------------------------------------
    # Function: _init_WelcomeMessage
    #
    # When the referee node is started the following is prompted to the user.
    #---------------------------------------------------------------------------
    def _init_WelcomeMessage(self):
        common_welcomeMsg("Referee", _VERSION)

    # --------------------------------------------------------------------------
    # Function: _init_getNumberOfPlayers
    #
    # Prompts the user for how many players will be in the game. After this
    # step, the user can start launching the player nodes as the referee message
    # will be sent.
    #
    # Note: If the max number of players in the referee settings is 2, then
    # this question will be skipped as it will assume there will be only 2
    # players.
    #---------------------------------------------------------------------------
    def _init_getNumberOfPlayers(self):
        print '---[ Game Configuration ]---'
        MaxNumberOfPlayers = _GAME_SETTINGS['MaxNumberOfPlayers']
        if MaxNumberOfPlayers == 2:
            self.SelectedNumOfPlayers = 2
        else:
            prompt = 'Please enter the number of players (2 to ' + str(MaxNumberOfPlayers) + ')'
            invalid = 'Invalid input, please select a value between 2 and ' + str(MaxNumberOfPlayers)
            outOfRange = 'Out of range, please enter a value between 2 and ' + str(MaxNumberOfPlayers)
            self.SelectedNumOfPlayers = int(common_checkInputRange(2, MaxNumberOfPlayers, prompt, invalid, outOfRange))

    # ==========================================================================
    #
    #
    # SET UP FUNCTIONS
    #
    #
    # ==========================================================================

    # --------------------------------------------------------------------------
    # Function: _setup_RefereeMessage
    #
    # Custom message to all robots in the game with the game information, score,
    # time, and player information such as role and scorePlayerInfoMsgs
    #---------------------------------------------------------------------------
    def _setup_RefereeMessage(self):
        log(_LVL['Debug'], 'Setting up message for referee to publish to all players...')

        # Setup publisher
        self.Refpub = rospy.Publisher("/robot_tag/Referee/RefereeMsg", RefereeMsg, queue_size=3)

        # Echo this information to other users
        self.Refmsg.Settings.NumberOfPlayers = self.SelectedNumOfPlayers
        self.Refmsg.Settings.DisableSoundEffects = _GAME_SETTINGS['DisableSoundEffects']
        self.Refmsg.Settings.PlayerNodeRateHz = _GAME_SETTINGS['PlayerNodeRateHz']
        self.Refmsg.Settings.PlayerMsgRateSec = _GAME_SETTINGS['PlayerMsgRateSec']

        # Default values for Game
        self.Refmsg.GameInfo.Round = 1
        self.Refmsg.GameInfo.StartTime = rospy.Time.now()
        self.Refmsg.GameInfo.RemainingTime = 0.0
        self.Refmsg.GameInfo.GameActive = False
        self.Refmsg.GameInfo.AbortReqActive = False

        # Setup players in hhe referee message
        for i in range(0, self.SelectedNumOfPlayers):
            data = PlayerInformation()
            data.Assignment = _PLAYERMODES['Transition']
            data.Score = 0
            data.ReadyToStart = 0
            data.NodeId = 0
            self.Refmsg.GameInfo.Player.append(data)

        # Set timer for transmission
        self.RefTimer = rospy.Timer(rospy.Duration(_REFEREE_NODE_SETTINGS['GameMsgRateSec']), self._cb_RefPublish)

    # --------------------------------------------------------------------------
    # Function: _setup_SubscriberMessages
    #
    # Sets up a list of subscriber nodes for each robot based on the number
    # of players
    #---------------------------------------------------------------------------
    def _setup_SubscriberMessages(self, NodeId):
        data = rospy.Subscriber("/robot_tag/Player" + str(NodeId) + "/PlayerMsg", PlayerMsg, self._cb_PlayerMsgRx)
        self.sub.append(data)

    # --------------------------------------------------------------------------
    # Function: _setup_GetPlayerStartingPose
    #
    # Gets the starting pose for the player based on their player ID
    #---------------------------------------------------------------------------
    def _setup_GetPlayerStartingPose(self, idx):
        PoseData = StartingPoseData()
        PoseData.x = _PLAYER_STARTING_POSE[self.CharSelectionList[idx].getPlayerId()][0]
        PoseData.y = _PLAYER_STARTING_POSE[self.CharSelectionList[idx].getPlayerId()][1]
        PoseData.theta = _PLAYER_STARTING_POSE[self.CharSelectionList[idx].getPlayerId()][2]
        return PoseData

    # ==========================================================================
    #
    #
    # CALLBACK FUNCTIONS
    #
    #
    # ==========================================================================

    # --------------------------------------------------------------------------
    # Function: _cb_RefPublish
    #
    # This function publishes the referee message at the given rate by the
    # configuration set by the user.
    # See http://wiki.ros.org/rospy/Overview/Time for other options for the
    # event argument.
    #---------------------------------------------------------------------------
    def _cb_RefPublish(self, event):
        # Send message to update robots
        #log(_LVL['Debug'], 'Referee Message Sent...')
        self.Refpub.publish(self.Refmsg)

    # --------------------------------------------------------------------------
    # Function: _cb_PlayerMsgRx
    #
    # Data received by the robots in the field. The PlayerID identifies the
    # content of the message.
    #---------------------------------------------------------------------------
    def _cb_PlayerMsgRx(self, data):
        #log(_LVL['Debug'], 'Message received from Player' + str(data.PlayerId))
        self.playerReady(data.PlayerId, data.ReadyToStart) # Update player status

    # ==========================================================================
    #
    #
    # SERVICE CALLBACK FUNCTIONS
    #
    #
    # ==========================================================================

    # --------------------------------------------------------------------------
    # Function: _srv_GetPlayerIdentity
    #
    # Service handler for a player node asking for its identity. If the request
    # is after the max number of characters is reached an exception will be
    # raised which will stop the requester's node execution.
    #---------------------------------------------------------------------------
    def _srv_GetPlayerIdentity(self, request):
        if len(self.CharSelectionList) < self.SelectedNumOfPlayers:
            prompt = "A new challenger has entered the arena, please select their personality:\r\n"
            tmpCharList = []
            for i in self.CharSelectionList:
                tmpCharList.append(i.getCharacterIdx())
            for key, value in _CHARACTER_NAME.iteritems():
                if key not in tmpCharList:
                    prompt += str((key+1)) + ') ' + str(value) + '\r\n'
            selectionCheck = True
            while(selectionCheck == True):
                characterIdx = int(common_checkInputRange(1, len(_CHARACTER_NAME)+1, prompt)) - 1 # Remove the offset
                if len(self.CharSelectionList) > 0:
                    for i in self.CharSelectionList:
                        if characterIdx == i.getCharacterIdx():
                            prompt = "Character already selected, please chose another."
                        else:
                            selectionCheck = False
                else:
                    selectionCheck = False

            tmp = playerIdClass()
            tmp.setNodeId(request.NodeId)
            tmp.setCharacterIdx(characterIdx)
            tmp.setPlayerId(len(self.CharSelectionList))
            self.CharSelectionList.append(tmp)
            print 'Please welcome ' + str(_CHARACTER_NAME[tmp.getCharacterIdx()]) + '!'

            # Look up the starting position for the player
            PoseData = self._setup_GetPlayerStartingPose(tmp.getPlayerId())

            # Set up player message subscription
            self._setup_SubscriberMessages(tmp.getNodeId())

            # Return data to the player
            return GetPlayerIdentityResponse((len(self.CharSelectionList)-1), tmp.getCharacterIdx(), PoseData)
        else:
            # Raise an exception and return none for the values
            log(_LVL['Warning'], "Player identity request made when the max number of players has been reached.")
            raise rospy.ServiceException
            return None

    # ==========================================================================
    #
    #
    # GAME OPERATION FUNCTIONS
    #
    #
    # ==========================================================================

    # --------------------------------------------------------------------------
    # Function: _gameOp_startGame
    #
    # Internal class function. This is used to start the game by getting the
    # current time and setting the game active signal to true.
    #---------------------------------------------------------------------------
    def _gameOp_startGame(self):
        # First time this is called, set up initial assignments
        for i in range(0, self.SelectedNumOfPlayers):
            if i == 0:
                self.playerAssignMode(i, _PLAYERMODES['Pursuer'])
                self.CurrentPursuer = i
            else:
                self.playerAssignMode(i, _PLAYERMODES['Evader'])
        self.Refmsg.GameInfo.GameActive = True
        self.GameStartControl = True
        self._gameOp_startRound()

    # --------------------------------------------------------------------------
    # Function: _gameOp_startRound
    #
    # Internal class function. This is used to start the game by getting the
    # current time and setting the game active signal to true.
    #---------------------------------------------------------------------------
    def _gameOp_startRound(self):
        # Set game clock
        self._playSound(self.BellSoundPath, False)
        CurrentTime = rospy.Time.now()
        print 'Round: ' + str(self.Refmsg.GameInfo.Round) + ' starts now!'
        log(_LVL['Debug'], 'Game started at: ' + str(CurrentTime))
        self.Refmsg.GameInfo.StartTime = CurrentTime
        self.Refmsg.GameInfo.RemainingTime = _GAME_SETTINGS['RoundLengthSecs']
        self.Refmsg.GameInfo.RoundActive = True

    # --------------------------------------------------------------------------
    # Function: _gameOp_endRound
    #
    # Internal class function. This function is used to end the round.
    #---------------------------------------------------------------------------
    def _gameOp_endRound(self):
        log(_LVL['Debug'], 'Round ' + str(self.Refmsg.GameInfo.Round) + ' Over!')
        self.Refmsg.GameInfo.RoundActive = False
        self.Refmsg.GameInfo.RemainingTime = 0.0

        for i in range(0, self.SelectedNumOfPlayers):
            if self.Refmsg.GameInfo.Player[i].Assignment == _PLAYERMODES['Evader']:
                self.Refmsg.GameInfo.Player[i].Score += 1
            self.playerAssignMode(i, _PLAYERMODES['Transition'])

        # Time is up, end the round or game
        if self.Refmsg.GameInfo.Round >= _GAME_SETTINGS['NumberOfRounds']:
            self._gameOp_stopGame(True)
        else:
            self.Refmsg.GameInfo.Round += 1
            self.CurrentPursuer += 1 # Set next pursuer

            # Set robots back to starting point and wait until that is complete.
            while (self.refereePlayersReadyChk() == True):
                continue

            # Assign new roles and start the next round
            for i in range(0, self.SelectedNumOfPlayers):
                self.playerSwitchModes(i)
            self._gameOp_startRound()

    # --------------------------------------------------------------------------
    # Function: _gameOp_stopGame
    #
    # Internal class function. This function is used to stop the game, halt
    # the remaning time clock, and assign all players back to neutral.
    #---------------------------------------------------------------------------
    def _gameOp_stopGame(self, results=False):
        log(_LVL['Debug'], 'Game Over!!!')
        # Set all bots to neutral
        self.Refmsg.GameInfo.GameActive = False
        for i in range(0, self.SelectedNumOfPlayers):
            self.playerAssignMode(i, 0)

        if results == True:
            scoreData = []
            for i in range(0, len(self.Refmsg.GameInfo.Player)):
                playerData = []
                playerData.append(i)
                playerData.append(self.Refmsg.GameInfo.Player[i].Score)
                scoreData.append(playerData)
            log(_LVL['Debug'], scoreData)
            common_resultsMsg(scoreData)

        # Play out the show
        self._playSound(self.GameOverSoundPath, True)

    # ==========================================================================
    #
    #
    # PLAYER OPERATION FUNCTIONS
    #
    #
    # ==========================================================================

    # --------------------------------------------------------------------------
    # Function: playerSwitchModes
    #
    # Switches modes for players from evader to pursuer
    #---------------------------------------------------------------------------
    def playerSwitchModes(self, player):
        if player == self.CurrentPursuer:
            log(_LVL['Debug'], 'Change Player ' + str(player) + ' from ' + str(self.Refmsg.GameInfo.Player[player].Assignment) + ' to ' + str(_PLAYERMODES['Pursuer']))
            self.Refmsg.GameInfo.Player[player].Assignment = _PLAYERMODES['Pursuer']
        else:
            log(_LVL['Debug'], 'Change Player ' + str(player) + ' from ' + str(self.Refmsg.GameInfo.Player[player].Assignment) + ' to ' + str(_PLAYERMODES['Evader']))
            self.Refmsg.GameInfo.Player[player].Assignment = _PLAYERMODES['Evader']

    # --------------------------------------------------------------------------
    # Function: playerScored
    #
    # Increments the score of the player that scored in the referee message
    #---------------------------------------------------------------------------
    def playerScored(self, player):
        log(_LVL['Debug'], 'Player ' + str(player) + ' scored!!')
        if player < self.SelectedNumOfPlayers:
            self.Refmsg.GameInfo.Player[player].Score += 1
        else:
            log(_LVL['Error'], 'Error: Player ' + str(player) + ' does not exist')

    # --------------------------------------------------------------------------
    # Function: playerAssignMode
    #
    # Assigns the player the game mode selected by the referee.
    #---------------------------------------------------------------------------
    def playerAssignMode(self, player, mode):
        log(_LVL['Debug'], 'Player ' + str(player) + ' assigned mode ' + str(mode))
        if mode in _PLAYERMODES:
            if player < self.SelectedNumOfPlayers:
                self.Refmsg.GameInfo.Player[player].Assignment = _PLAYERMODES[mode]
            else:
                log(_LVL['Error'], 'Error: Player ' + str(player) + ' does not exist')
        else:
            log(_LVL['Error'], 'Mode: ' + str(mode) + ' does not exist for player ' + str(player) )

    # --------------------------------------------------------------------------
    # Function: playerReady
    #
    # Status from the robot and if they are ready or not.
    #---------------------------------------------------------------------------
    def playerReady(self, player, status):
        log(_LVL['Debug'], 'Player ' + str(player) + ' status is: ' + str(status))
        if player < self.SelectedNumOfPlayers:
            self.Refmsg.GameInfo.Player[player].ReadyToStart = status
        else:
            log(_LVL['Error'], 'Error: Player ' + str(player) + ' does not exist')

    # --------------------------------------------------------------------------
    # Function: refereePlayersReadyChk
    #
    # Checks all the players if they are ready to start teh round.
    #---------------------------------------------------------------------------
    def refereePlayersReadyChk(self):
        playersReady = True # Assume true until a player says they are not ready
        for player in self.Refmsg.GameInfo.Player:
            if player.ReadyToStart == 0:
                playersReady = False
        return playersReady

    # --------------------------------------------------------------------------
    # Function: _player_PlayerCaptured
    #
    # Handler for when the player is captured.
    #---------------------------------------------------------------------------
    def _player_PlayerCaptured(self, NodeId):
        SrvStr = '/robot_tag/Player' + str(NodeId) + '/PlayerCaptured'
        rospy.wait_for_service(SrvStr)
        try:
            # Send notification to that you are captured
            PCSrvReq = rospy.ServiceProxy(SrvStr, PlayerCaptured)
            response = PCSrvReq(playerId)

            PCSrvReq.close() # Close client since it is not needed anymore
        except rospy.ServiceException, e:
            log(_LVL['Error'], "Service call failed: %s" % e)


    # ==========================================================================
    #
    #
    # MISC FUNCTIONS
    #
    #
    # ==========================================================================

    # --------------------------------------------------------------------------
    # Function: _shutdown_hook
    #
    # Shutdown hook. Currently not used since it only kills the robot tag
    # remotely and does not stop the robot.
    #---------------------------------------------------------------------------
    def _shutdown_hook(self):
        log(_LVL['Info'], 'Shutting down...')
        '''
        if len(self.CharSelectionList) > 0:
            for idx in range (0, self.SelectedNumOfPlayers):
                ServiceStr = '/robot_tag/Player' + str(self.CharSelectionList[idx].getNodeId()) + '/AbortPlayer'
                rospy.wait_for_service(ServiceStr)
                try:
                    APSrvReq = rospy.ServiceProxy(ServiceStr, AbortPlayer)
                    # Send notification to that you are captured
                    response = APSrvReq()
                    APSrvReq.close() # Close client since it is not needed anymore
                except rospy.ServiceException, e:
                    log(_LVL['Error'], "Service call failed: %s" % e)
        else:
            pass
        '''

    # --------------------------------------------------------------------------
    # Function: _playSound
    #
    # Plays sound effects for the referee node.
    #---------------------------------------------------------------------------
    def _playSound(self, path, block=False):
        if _GAME_SETTINGS['DisableSoundEffects'] == False:
            soundhandle = SoundClient(blocking=block, topic="/robot_tag/Referee/")
            rospy.sleep(3)

            # Build path to sound file
            filename = random.choice(dircache.listdir(path))

            # Play sound file
            log(_LVL['Debug'], 'Playing file: ' + filename)
            soundhandle.playWave(os.path.join(path, filename), 1.0)
            rospy.sleep(1)
        else:
            log(_LVL['Warning'], 'Sounds are currently disabled')


    # ==========================================================================
    #
    #
    # MAIN FUNCTIONS
    #
    #
    # ==========================================================================

    # --------------------------------------------------------------------------
    # Function: startReferee
    #
    # After the referee has been initalized, this function is started and starts
    # the rules processing for each round.
    #---------------------------------------------------------------------------
    def startReferee(self):
        while not rospy.is_shutdown():
            # ------------
            # Pre-Game
            # ------------
            if (self.Refmsg.GameInfo.GameActive == False) and (self.refereePlayersReadyChk() == False):
                #log(_LVL['Debug'], 'Players not ready...') # Do nothing
                pass
            elif (self.Refmsg.GameInfo.GameActive == False) and (self.refereePlayersReadyChk() == True) and (self.GameStartControl == False):
                self._gameOp_startGame()
            else:
                # Update Remaning time
                if self.Refmsg.GameInfo.GameActive == True:
                    # Remaning time is current time from the start time in seconds
                    newRemainingTime = ( self.Refmsg.GameInfo.StartTime.to_sec() + \
                                         _GAME_SETTINGS['RoundLengthSecs'] ) - \
                                         rospy.Time.now().to_sec()
                    if (newRemainingTime) > 0:
                        self.Refmsg.GameInfo.RemainingTime = round(newRemainingTime)
                    else:
                        self._gameOp_endRound()

            self.Rate.sleep()
        else:
            self._gameOp_stopGame(False)

# ===[ EOF ]====================================================================

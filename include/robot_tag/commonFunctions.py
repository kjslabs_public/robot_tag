#!/usr/bin/env python
# ==============================================================================
# ECE 491
# Robot Tag - CommonFunctions.py
# Authors: Kevin Smith (smit2958@msu.edu),
#          Scott Knaub (knaubsco@msu.edu),
#          Sam Campbell (campb980@msu.edu),
#          Alisha Jacob (jacobali@msu.edu)
#
# This file is shared between the referee nodes and the player nodes. Any
# changes to this file needs to be also made on each of the player nodes as the
# data here is not broadcasted between nodes.
# ==============================================================================

# ===[ IMPORTS ]================================================================
import rospy
from os import system, name

# ===[ DICTIONARIES ]===========================================================
# ------------------------------------------------------------------------------
# Dictionary: _LOGLEVEL
#
# This dictionary provies an interface for the log function defined in this file.
#-------------------------------------------------------------------------------
_LOGLEVEL = {
    "Debug": 0,
    "Info": 1,
    "Warning": 2,
    "Error": 3,
    "Fatal": 4
}

# ===[ FUNCTIONS ]==============================================================
# ------------------------------------------------------------------------------
# Function: common_checkInputRange
#
# This function prompts the user with the prompt and checks if the input received
# is between the min and max values provided. Alternate invalid and out of range
# return statements can be provided or generic ones can be used. This function
# will halt node execution until user input is passed or the node is shutdown.
#
# Note: this function is assuming an integer selection range.
#-------------------------------------------------------------------------------
def common_checkInputRange(min, max, prompt, invalid="", outOfRange=""):
    if invalid == "":
        invalid = 'Invalid input, please select a value between ' + str(min) + ' and ' + str(max)
    if outOfRange == "":
        outOfRange = 'Out of range, please enter a value between ' + str(min) + ' and ' + str(max)

    print str(prompt)
    while True and not rospy.is_shutdown():
        try:
            input = int(raw_input())
        except ValueError:
            print str(invalid)
        else:
            if (input in range(min, (max + 1))):
                break # Break out of the prompt
            else:
                print str(outOfRange)
    return input

# ------------------------------------------------------------------------------
# Function: common_clearTermScreen
#
# This function clears the terminal screen
#-------------------------------------------------------------------------------
def common_clearTermScreen():
    # Clear terminal window
    if name == 'nt':  # Windows
        system('cls')
    else: # Mac and linux
        system('clear')

# ------------------------------------------------------------------------------
# Function: common_welcomeMsg
#
# This function displays a welcome message for the node and the version.
#-------------------------------------------------------------------------------
def common_welcomeMsg(node, version):
    common_clearTermScreen()

    print """
  ____       _           _     _____           _
 |  _ \ ___ | |__   ___ | |_  |_   _|_ _  __ _| |TM
 | |_) / _ \| '_ \ / _ \| __|   | |/ _` |/ _` | |
 |  _ < (_) | |_) | (_) | |_    | | (_| | (_| |_|
 |_| \_\___/|_.__/ \___/ \__|   |_|\__,_|\__, (_)
       --- The Game of Champions ---     |___/
"""
    print '-----------------------------------------------'
    print node + ' Node -- Ver: ' + str(version)
    print ''

# ------------------------------------------------------------------------------
# Function: common_resultsMsg
#
# This function displays the results after all the rounds are completed.
#-------------------------------------------------------------------------------
def common_resultsMsg(scores):
    common_clearTermScreen()

    print """
  ____                 _ _
 |  _ \ ___  ___ _   _| | |_ ___
 | |_) / _ \/ __| | | | | __/ __|
 |  _ <  __/\__ \ |_| | | |_\__ \\
 |_| \_\___||___/\__,_|_|\__|___/

"""
    print '-----------------------------------------------'
    for i in scores:
        print 'Player ID: ' + str(i[0] )+ ' -- Score: ' + str(i[1])

# ------------------------------------------------------------------------------
# Function: common_log
#
# This function abstracts the logging provided by rospy.
#-------------------------------------------------------------------------------
def common_log(level=1, msg="", *args, **kwargs):
    if level == 1:
        rospy.loginfo(msg, *args, **kwargs)
    elif level == 2:
        rospy.logwarn(msg, *args, **kwargs)
    elif level == 3:
        rospy.logerr(msg, *args, **kwargs)
    elif level == 4:
        rospy.logfatal(msg, *args, **kwargs)
    else:
        rospy.logdebug(msg, *args, **kwargs)

# ===[ EOF ]====================================================================

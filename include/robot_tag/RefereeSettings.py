#!/usr/bin/env python
# ==============================================================================
# ECE 491
# Robot Tag - RefereeSettings.py
# Authors: Kevin Smith (smit2958@msu.edu),
#          Scott Knaub (knaubsco@msu.edu),
#          Sam Campbell (campb980@msu.edu),
#          Alisha Jacob (jacobali@msu.edu)
#
# This file is only used by the referee node. The data contained within this
# file shall be transmitted via the referee messages that are sent to the
# other nodes, or not at all.
# ==============================================================================

# ===[ IMPORTS ]================================================================


# ===[ DICTIONARIES ]===========================================================
# ------------------------------------------------------------------------------
# Dictionary: _CHARACTER_NAME
#
# This dictionary contains the names of the players configured for this game.
# These indexes shall also corrispond to the directory paths for the sound
# effects that player node will use.
#-------------------------------------------------------------------------------
_CHARACTER_NAME = {
    0: 'Macho Man Randy Savage',
    1: 'Hulk Hogan',
    2: 'Rick Fliar',
    3: 'The Rock'
}

# ------------------------------------------------------------------------------
# Dictionary: _REFEREE_NODE_SETTINGS
#
# This dictionary contains the settings for the referee node only.
#-------------------------------------------------------------------------------
_REFEREE_NODE_SETTINGS = {
    'NodeRateHz'      : 20,
    'GameMsgRateSec'  : 1
}

# ------------------------------------------------------------------------------
# Dictionary: _GAME_SETTINGS
#
# This dictionary contains the settings for the game and is transmitted to the
# other nodes so only the referee node needs to be modified for rule changes.
#-------------------------------------------------------------------------------
_GAME_SETTINGS = {
    'RoundLengthSecs'     : 15,
    'NumberOfRounds'      : 2,
    'MaxNumberOfPlayers'  : 4,
    'NumberOfCharacters'  : 2,
    'DisableSoundEffects' : False,
    'PlayerNodeRateHz'    : 40,
    'PlayerMsgRateSec'    : 1
}

# ------------------------------------------------------------------------------
# List: _PLAYER_STARTING_POSE
#
# This list conatins the starting X, Y, and theta to be used to issue the
# starting point for each player.
#-------------------------------------------------------------------------------
_PLAYER_STARTING_POSE = [
    [0.0, 0.0, 0.0],
    [1.0, 1.0, 1.0],
    [2.0, 2.0, 2.0],
    [3.0, 3.0, 3.0]
]

# ===[ EOF ]====================================================================

#!/usr/bin/env python
# ==============================================================================
# ECE 491
# Robot Tag - Player.py
# Authors: Kevin Smith (smit2958@msu.edu),
#          Scott Knaub (knaubsco@msu.edu),
#          Sam Campbell (campb980@msu.edu),
#          Alisha Jacob (jacobali@msu.edu)
#
# This file contains the player class.
#
# REQUIREMENTS: This file requires sound_play to be installed for ros.
# ==============================================================================

# ===[ IMPORTS ]================================================================
import rospy
import os
import sys
import random
import dircache
import actionlib
from math import sin, cos, sqrt

# Import Robot tag settings
from robot_tag.CommonSettings import _SOUND_EVENTS, _PLAYERMODES, _VERSION

# Import Robot tag functions, services, and messsages
from robot_tag.CommonFunctions import _LOGLEVEL as _LVL, common_log as log, common_welcomeMsg
from robot_tag.msg import RefereeMsg, PlayerMsg
from robot_tag.srv import GetPlayerIdentity, GetPlayerIdentityResponse
from robot_tag.srv import PlayerCaptured, PlayerCapturedResponse
from robot_tag.srv import AbortPlayer, AbortPlayerResponse

# Import sound play messages and library support.
from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient

# ===[ CLASSES ]================================================================
class Player(object):
    # --------------------------------------------------------------------------
    # Function: __init__
    #
    # This is the constructor of the player class. The local variables
    # and message information is defined here.
    #---------------------------------------------------------------------------
    def __init__(self, NodeId):
        self.player = GetPlayerIdentityResponse()
        self.CharacterAssigned = False
        self.NodeId = NodeId

        self.Rate = None

        self.SoundPath = None
        self.IntroPlayed = False
        self.EvaderTauntPlayed = False
        self.PlayerCaptured = False
        self.PlayerCapturedSoundPlayed = False

        self.PlayerPub = None
        self.PlayerMsg = PlayerMsg()
        self.PlayerTimer = None

        self.APsrv = None
        self.AbortActive = False
        self.TransitionComplete = False

        self.RefSub = rospy.Subscriber('/robot_tag/Referee/RefereeMsg', RefereeMsg, self._cb_RefMsg)
        self.RefMsg = RefereeMsg()
        self.name = ""
        self.PISrvCallMade = False
        self.PCsrv = None

    # ==========================================================================
    #
    #
    # INIT FUNCTIONS
    #
    #
    # ==========================================================================

    # --------------------------------------------------------------------------
    # Function: init
    #
    # This function shall be called after the class is created to the player.
    #---------------------------------------------------------------------------
    def init(self):
        common_welcomeMsg("Player", _VERSION)
        print "---[ Waiting for player assignment from referee ]---"
        while (self.CharacterAssigned == False) and not rospy.is_shutdown():
            if self.CharacterAssigned == True:
                # Player Intro
                if self.IntroPlayed == False:
                    self.IntroPlayed = True
                    # Play intro sound for the player
                    log(_LVL['Debug'], "Playing player intro...")
                    self._player_Intro()
        else:
            log(_LVL['Debug'], "Node ID: " + str(self.NodeId) + " Player ID: " + str(self.player.PlayerId) + " Character Idx: " + str(self.player.CharacterIndex))
        # Init completed, start player
        self.startPlayer()

    # --------------------------------------------------------------------------
    # Function: _init_MoveToStartingPos
    #
    # Moves the player to their starting location
    #---------------------------------------------------------------------------
    def _init_MoveToStartingPos(self):
        print "---[ Moving to Starting Position: " + str(self.player.Pose.x) + ', ' + str(self.player.Pose.y) + ', ' + str(self.player.Pose.theta) + ' ]---'
        # For demostration only! This is just to delay the player from saying they are ready.
        # In the real game, this would be reaplced with giving the starting point to the navigation stack
        rospy.sleep(10)
        # ----------
        print "---[ Move Complete ]---"
        return False

    # --------------------------------------------------------------------------
    # Function: _init_GetPlayerIdentityClient
    #
    # Call to the referee for the player assignment information
    #---------------------------------------------------------------------------
    def _init_GetPlayerIdentityClient(self):
        rospy.wait_for_service('/robot_tag/Referee/GetPlayerIdentity')
        try:
            PISrvReq = rospy.ServiceProxy('/robot_tag/Referee/GetPlayerIdentity', GetPlayerIdentity)

            # Get result and assign information
            try:
                self.player = PISrvReq(int(self.NodeId))
                self.CharacterAssigned = True

                # Assign sound path
                tmpSoundPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), "../sounds/")
                self.SoundPath = os.path.join(tmpSoundPath, str(self.player.CharacterIndex))

                # Set up message publisher
                self.PlayerPub = rospy.Publisher("/robot_tag/Player" + str(self.NodeId) + "/PlayerMsg", PlayerMsg, queue_size=3)

                # Create service handler to give notice they were captured.
                self.PCsrv = rospy.Service("/robot_tag/Player" + str(self.NodeId) + "/PlayerCaptured", PlayerCaptured, self._srv_PlayerCaptured)

                # Set Player ID for player message
                self.PlayerMsg.PlayerId = self.player.PlayerId

                # Set up abort service
                self.APsrv = rospy.Service("/robot_tag/Player" + str(self.NodeId) + "/AbortPlayer", AbortPlayer, self._srv_AbortPlayer)

                # Set timer for transmission
                self.PlayerTimer = rospy.Timer(rospy.Duration(self._settings_PlayerMsgRateSec()), self._cb_PlayerMsgPublish)

                # Close client since it is not needed anymore
                PISrvReq.close()
            except rospy.ServiceException as exc:
                log(_LVL['Error'], "Max number of playes alrady registered. Wait until next game!")
                log(_LVL['Debug'], str(exc))
                rospy.signal_shutdown("Good Bye!")
        except rospy.ServiceException, e:
            log(_LVL['Error'], "Service call failed: %s" % e)

# ==========================================================================
#
#
# SET UP FUNCTIONS
#
#
# ==========================================================================


    # ==========================================================================
    #
    #
    # CALLBACK FUNCTIONS
    #
    #
    # ==========================================================================

    # --------------------------------------------------------------------------
    # Function: _cb_RefMsg
    #
    # Callback triggered when the referee message is received. This also starts
    # the entire player node.
    #---------------------------------------------------------------------------
    def _cb_RefMsg(self, msg):
        # Write the value
        self.RefMsg = msg

        # Get settings from the referee message.
        if self.Rate == None:
            self.Rate = rospy.Rate(self._settings_PlayerNodeRateHz())

        if self.CharacterAssigned == False and self.PISrvCallMade == False:
            # Character is not assigned yet, and the referee node is up, send request
            self.PISrvCallMade = True
            self._init_GetPlayerIdentityClient()

    # --------------------------------------------------------------------------
    # Function: _cb_PlayerMsgPublish
    #
    # Callback to publish the player message.
    #---------------------------------------------------------------------------
    def _cb_PlayerMsgPublish(self, event):
        #log(_LVL['Debug'], "Sending player message...")
        self.PlayerPub.publish(self.PlayerMsg)

    # ==========================================================================
    #
    #
    # SERVICE CALLBACK FUNCTIONS
    #
    #
    # ==========================================================================

    # --------------------------------------------------------------------------
    # Function: _srv_PlayerCaptured
    #
    # Call out from the referee that the player was captured.
    #---------------------------------------------------------------------------
    def _srv_PlayerCaptured(self, request):
        if request.PlayerId == _getPlayerId():
            self.PlayerCaptured = True

    # --------------------------------------------------------------------------
    # Function: _srv_AbortPlayer
    #
    # Used with the shutdown hook for aborting the node.
    #---------------------------------------------------------------------------
    def _srv_AbortPlayer(self, request):
        log(_LVL['Error'], 'Abort received...halting node!')
        self.AbortActive = True
        return AbortPlayerResponse(1)

    # ==========================================================================
    #
    #
    # HELPING FUNCTIONS
    # The following functions just simply the code above.
    #
    # ==========================================================================

    def _getPlayerId(self):
        return self.player.PlayerId

    def _getRoundActive(self):
        return self.RefMsg.GameInfo.RoundActive

    def _getPlayerAssignment(self):
        return int(self.RefMsg.GameInfo.Player[self._getPlayerId()].Assignment)

    def _setReadyToStart(self, value):
        self.PlayerMsg.ReadyToStart = value

    def _player_EvaderTaunt(self):
        self._playSound(_SOUND_EVENTS['EvaderTaunt'])

    def _player_PursuerTaunt(self):
        self._playSound(_SOUND_EVENTS['PursuerTaunt'])

    def _player_Intro(self):
        self._playSound(_SOUND_EVENTS['Intro'], False)

    def _player_Captured(self):
        self._playSound(_SOUND_EVENTS['Captured'])

    def _playSound(self, type, block=False, playerId=None):
        if playerId is None:
            playerId = self.player.PlayerId

        if self._settings_DisableSoundEffects() == False:
            soundhandle = SoundClient(blocking=block, topic='/robot_tag/Player' + self.NodeId + '/')

            rospy.sleep(3)

            # Build path to sound file
            dir = os.path.join(self.SoundPath, type)
            filename = random.choice(dircache.listdir(dir))

            # Play sound file
            log(_LVL['Debug'], 'Playing file: ' + filename)
            soundhandle.playWave(os.path.join(dir, filename), 1.0)
            rospy.sleep(1)
        else:
            log(_LVL['Warning'], 'Sounds are currently disabled')

    def _settings_DisableSoundEffects(self):
        return self.RefMsg.Settings.DisableSoundEffects

    def _settings_NumberOfPlayers(self):
        return self.RefMsg.Settings.NumberOfPlayers

    def _settings_PlayerMsgRateSec(self):
        return self.RefMsg.Settings.PlayerMsgRateSec

    def _settings_PlayerNodeRateHz(self):
        return self.RefMsg.Settings.PlayerNodeRateHz

    # ==========================================================================
    #
    #
    # MAIN FUNCTIONS
    #
    #
    # ==========================================================================

    # --------------------------------------------------------------------------
    # Function: startPlayer
    #
    # Main function of this class. Called after setups are complete.
    #---------------------------------------------------------------------------
    def startPlayer(self):
        while True and not rospy.is_shutdown():
            if self.AbortActive == True:
                rospy.signal_shutdown("Aborting...")

            if self._getPlayerAssignment() == _PLAYERMODES['Transition']:
                if self.TransitionComplete == False:
                    # Moving to starting spot
                    while self._init_MoveToStartingPos():
                        pass
                    else:
                        self.TransitionComplete = True
                        self._setReadyToStart(True)
            else:
                self.TransitionComplete = False # Clear the flag so when the round ends the robots return to start

                if (self.PlayerCaptured == True) and (self._getPlayerAssignment() == _PLAYERMODES['Neutral']):
                    log(_LVL['Debug'], 'Player is neutral...')
                    self._setReadyToStart(False)
                    if self.PlayerCapturedSoundPlayed == False:
                        self.PlayerCapturedSoundPlayed = True
                        self._player_Captured()
                else:
                    if self._getRoundActive() == 1:
                        self.TransitionComplete = False # Set back to false for next round
                        if self._getPlayerAssignment() == _PLAYERMODES['Pursuer']:
                            if self.EvaderTauntPlayed == False:
                                self._player_PursuerTaunt()
                                self.EvaderTauntPlayed = True
                        elif self._getPlayerAssignment() == _PLAYERMODES['Evader']:
                            pass
                    else:
                        self._setReadyToStart(False)
            self.Rate.sleep()

# ===[ EOF ]====================================================================

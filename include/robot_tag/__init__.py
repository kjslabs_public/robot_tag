#!/usr/bin/env python
# ==============================================================================
# ECE 491
# Robot Tag - __init__.py
# Authors: Kevin Smith (smit2958@msu.edu),
#          Scott Knaub (knaubsco@msu.edu),
#          Sam Campbell (campb980@msu.edu),
#          Alisha Jacob (jacobali@msu.edu)
#
# This file is called by catkin make to ensure the package and the include
# folder is included when the code is executed.
#
# IMPORTANT: Do NOT manually invoke this file. Use the command catkin_make
# instead.
# ==============================================================================

# ===[ IMPORTS ]================================================================

# File is intentially left blank

# ===[ EOF ]====================================================================

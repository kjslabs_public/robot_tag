#!/usr/bin/env python
# ==============================================================================
# ECE 491
# Robot Tag - CommonSettings.py
# Authors: Kevin Smith (smit2958@msu.edu),
#          Scott Knaub (knaubsco@msu.edu),
#          Sam Campbell (campb980@msu.edu),
#          Alisha Jacob (jacobali@msu.edu)
#
# This file is shared between the referee nodes and the player nodes. Any
# changes to this file needs to be also made on each of the player nodes as the
# data here is not broadcasted between nodes.
# ==============================================================================

# ===[ IMPORTS ]================================================================
import rospy

_VERSION = "1.0.0"

# Enables the debugging levels in the nodes. Uncomment which one is needed
#_DEBUG_LEVEL = rospy.DEBUG
_DEBUG_LEVEL = rospy.INFO #Default
#_DEBUG_LEVEL = rospy.WARN
#_DEBUG_LEVEL = rospy.ERROR
#_DEBUG_LEVEL = rospy.FATAL

# ===[ DICTIONARIES ]===========================================================
# ------------------------------------------------------------------------------
# Dictionary: _SOUND_EVENTS
#
# This dictionary contains the type of sound events used in this game. It
# is indexed by the player identities that are located in the RefereeSettings
# file.
#-------------------------------------------------------------------------------
_SOUND_EVENTS = {
    'Intro'         : 'Intro/',
    'EvaderTaunt'   : 'EvaderTaunt/',
    'PursuerTaunt'  : 'PursuerTaunt/',
    'Captured'      : 'Captured/'
}

# ------------------------------------------------------------------------------
# Dictionary: _PLAYERMODES
#
# This dictionary contains the modes used within the game.
#   Neutral - Robot stop all functions and movement
#   Evader - Robot try to evade from the pursuer robot
#   Pursuer - Robot that is trying to trap an evader robot.
#   Transition - Sends the robot to their starting position
#-------------------------------------------------------------------------------
_PLAYERMODES = {
    0 : 0,
    'Neutral'    : 0,
    1 : 1,
    'Evader'     : 1,
    2 : 2,
    'Pursuer'    : 2,
    3 : 3,
    'Transition' : 3
}

# ===[ EOF ]====================================================================

#!/usr/bin/env python
import rospy
import actionlib
import math
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from actionlib_msgs.msg import *
from geometry_msgs.msg import Pose, Point, Quaternion
from tf.transformations import quaternion_from_euler
from random import choice


class robot_coordinate():
    def __init__(self):
        self.x = None
	self.y = None

    def __str__(self):
        return str(vars(self))





class robot_goal_logic():
    def __init__(self):

#       self.goal_sent = False

#	self.pursuer_pos.x = None
#	self.pursuer_pos.y = None

	self.quad_checkpoint = [[0.15,0.15], [2.25,0.15], [2.25,2.25], [0.15,2.25]]

	self.distlist =[]
 	self.evader_point_list =[][]
	self.evader_dist_list = []
	self.num_of_evaders = 0
	self.looptime = 2

	self.quad1 = [[[0.15,0.15],[0.3,0.15],[0.45,0.15],[0.6,0.15],[0.75,0.15],[0.9,0.15],[1.05,0.15],[1.2,0.15]],
		[[0.15,0.3],[0.3,0.3],[0.45,0.3],[0.6,0.3],[0.75,0.3],[0.9,0.3],[1.05,0.3],[1.2,0.3]],
		[[0.15,0.45],[0.3,0.45],[0.45,0.45],[0.6,0.45],[0.75,0.45],[0.9,0.45],[1.05,0.45],[1.2,0.45]],
		[[0.15,0.6],[0.3,0.6],[0.45,0.6],[0.6,0.6],[0.75,0.6],[0.9,0.6],[1.05,0.6],[1.2,0.6]],
		[[0.15,0.75],[0.3,0.75],[0.45,0.75],[0.6,0.75],[0.75,0.75],[0.9,0.75],[1.05,0.75],[1.2,0.75]],
		[[0.15,0.9],[0.3,0.9],[0.45,0.9],[0.6,0.9],[0.75,0.9],[0.9,0.9],[1.05,0.9],[1.2,0.9]],
		[[0.15,1.05],[0.3,1.05],[0.45,1.05],[0.6,1.05],[0.75,1.05],[0.9,1.05],[1.05,1.05],[1.2,1.05]],
		[[0.15,1.2],[0.3,1.2],[0.45,1.2],[0.6,1.2],[0.75,1.2],[0.9,1.2],[1.05,1.2],[1.2,1.2]]]

        self.offset = 1.05
        self.quad=[]
        for quad in range(0,4):
	    row_list =[]
            for row in self.quad1:
	        column_list = []
	        for point in row:
		    temp = robot_coordinate()
		    offsetx = 0
		    offsety = 0
		    if quad == 1:
		        offsetx = self.offset

		    elif quad == 2:
			offsetx = self.offset
			offsety = self.offset

		    elif quad == 3:
			offsety = self.offset


		    temp.x = point[0] + offsetx
		    temp.y = point[1] + offsety
		    column_list.append(temp)
                row_list.append(column_list)
	    self.quad.append(row_list)


'''
	quad2 = [[1.2,0.15],[1.35,0.15],[1.5,0.15],[1.65,0.15],[1.8,0.15],[1.95,0.15],[2.1,0.15],[2.25,0.15]]
		[[1.2,0.3],[1.35,0.3],[1.5,0.3],[1.65,0.3],[1.8,0.3],[1.95,0.3],[2.1,0.3],[2.25,0.3]]
		[[1.2,0.45],[1.35,0.45],[1.5,0.45],[1.65,0.45],[1.8,0.45],[1.95,0.45],[2.1,0.45],[2.25,0.45]]
		[[1.2,0.6],[1.35,0.6],[1.5,0.6],[1.65,0.6],[1.8,0.6],[1.95,0.6],[2.1,0.6],[2.25,0.6]]
		[[1.2,0.75],[1.35,0.75],[1.5,0.75],[1.65,0.75],[1.8,0.75],[1.95,0.75],[2.1,0.75],[2.25,0.75]]
		[[1.2,0.9],[1.35,0.9],[1.5,0.9],[1.65,0.9],[1.8,0.9],[1.95,0.9],[2.1,0.9],[2.25,0.9]]
		[[1.2,1.05],[1.35,1.05],[1.5,1.05],[1.65,1.05],[1.8,1.05],[1.95,1.05],[2.1,1.05],[2.25,1.05]]
		[[1.2,1.2],[1.35,1.2],[1.5,1.2],[1.65,1.2],[1.8,1.2],[1.95,1.2],[2.1,1.2],[2.25,1.2]]


	quad3 = [[1.2,1.2],[1.35,1.2],[1.5,1.2],[1.65,1.2],[1.8,1.2],[1.95,1.2],[2.1,1.2],[2.25,1.2]]
		[[1.2,1.35],[1.35,1.35],[1.5,1.35],[1.65,1.35],[1.8,1.35],[1.95,1.35],[2.1,1.35],[2.25,1.35]]
		[[1.2,1.5],[1.35,1.5],[1.5,1.5],[1.65,1.5],[1.8,1.5],[1.95,1.5],[2.1,1.5],[2.25,1.5]]
		[[1.2,1.65],[1.35,1.65],[1.5,1.65],[1.65,1.65],[1.8,1.65],[1.95,1.65],[2.1,1.65],[2.25,1.65]]
		[[1.2,1.8],[1.35,1.8],[1.5,1.8],[1.65,1.8],[1.8,1.8],[1.95,1.8],[2.1,1.8],[2.25,1.8]]
		[[1.2,1.95],[1.35,1.95],[1.5,1.95],[1.65,1.95],[1.8,1.95],[1.95,1.95],[2.1,1.95],[2.25,1.95]]
		[[1.2,2.1],[1.35,2.1],[1.5,2.1],[1.65,2.1],[1.8,2.1],[1.95,2.1],[2.1,2.1],[2.25,2.1]]
		[[1.2,2.25],[1.35,2.25],[1.5,2.25],[1.65,2.25],[1.8,2.25],[1.95,2.25],[2.1,2.25],[2.25,2.25]]

	quad4 = [[0.15,1.2],[0.3,1.2],[0.45,1.2],[0.6,1.2],[0.75,1.2],[0.9,1.2],[1.05,1.2],[1.2,1.2]]
		[[0.15,1.35],[0.3,1.35],[0.45,1.35],[0.6,1.35],[0.75,1.35],[0.9,1.35],[1.05,1.35],[1.2,1.35]]
		[[0.15,1.5],[0.3,1.5],[0.45,1.5],[0.6,1.5],[0.75,1.5],[0.9,1.5],[1.05,1.5],[1.2,1.5]]
		[[0.15,1.65],[0.3,1.65],[0.45,1.65],[0.6,1.65],[0.75,1.65],[0.9,1.65],[1.05,1.65],[1.2,1.65]]
		[[0.15,1.8],[0.3,1.8],[0.45,1.8],[0.6,1.8],[0.75,1.8],[0.9,1.8],[1.05,1.8],[1.2,1.8]]
		[[0.15,1.95],[0.3,1.95],[0.45,1.95],[0.6,1.95],[0.75,1.95],[0.9,1.95],[1.05,1.95],[1.2,1.95]]
		[[0.15,2.1],[0.3,2.1],[0.45,2.1],[0.6,2.1],[0.75,2.1],[0.9,2.1],[1.05,2.1],[1.2,2.1]]
		[[0.15,2.25],[0.3,2.25],[0.45,2.25],[0.6,2.25],[0.75,2.25],[0.9,2.25],[1.05,2.25],[1.2,2.25]]

'''

    def robot_goal(self, mode):
	if mode == 'pursuer':
		# Add Pursuer goal logic here

		# receive evader positions and determine distance from each.
		# Use nearest evader pos to calculate a new goal
		# newx = (evader.x + (cos(yaw_theta)*(evader.vel.x * looptime)))
		# newy = (evader.y + (sin(yaw_theta)*(evader.vel.x * looptime)))
		# Check if in range of areana and if not bound to accetable point
		# pass new goal to pursuer nav stack
		for i in self.num_of_evaders:

			self.evader_point_list.append([self.evader_pos.x, self.evader_pos.y])
			self.evader_dist_list.append((sqrt((local.x - self.evader_pos.x)**2 + (local.y - self.evader_pos.y)**2))


		min_dist = min(self.evader_dist_list)
		point_index = self.evader_dist_list.index(min_dist)
		target.x = self.evader_point_list[point_index][0]
		target.y = self.evader_point_list[point_index][1]

		newx = (target.x + (cos(yaw_theta)*(target.vel.x * self.looptime)))
		newy = (target.y + (sin(yaw_theta)*(target.vel.x * self.looptime)))

		if newx < 0.15:
			newx_bound = 0.15
		elif newx > 2.25:
			newx_bound = 2.25
		else:
			newx_bound = newx

		if newy < 0.15:
			newy_bound = 0.15
		elif newy > 2.25:
			newy_bound = 2.25
		else:
			newy_bound = newy

		new_pursue_goal = (newx_bound, newy_bound)



	else:
		self.dist_from_pursuer = sqrt((self.my_pos.x - self.pursuer_pos.x)**2 + (self.my_pos.y - self.pursuer_pos.y)**2)
		if self.dist_from_pursuer < 1.0:

			for quad in self.quad_checkpoint:

			    self.distlist.append((sqrt((quad[0] - self.pursuer_pos.x)**2 + (quad[1] - self.pursuer_pos.y)**2))


			max_quad = max(self.distlist)
			quad_index = self.distlist.index(max_quad)
			new_evade_goal = random.choice(self.quad[quad_index][random.choice(self.quad[quad_index])])






if __name__ == '__main__':
    try:

	new_goal = robot_goal_logic()
	for i in new_goal.quad:
	    for j in i:
		for k in j:
		    print k

#	rospy.loginfo("new goal", new_goal)

    except rospy.ROSInterruptException:
#        rospy.loginfo("Ctrl-C Process Interrupted")
	pass

#!/usr/bin/env python


import rospy
import actionlib
import math
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from actionlib_msgs.msg import *
from geometry_msgs.msg import Pose, Point, Quaternion
from tf.transformations import quaternion_from_euler

class tag_navigation():
    def __init__(self):

        rospy.init_node('tag_nav', anonymous=False)

        self.goal_sent = False

	# What to do if shut down (e.g. Ctrl-C or failure)
	rospy.on_shutdown(self.shutdown)
	
	# Tell the action client that we want to spin a thread by default
	self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)
	rospy.loginfo("Wait for the action server to begin")

	# Allow up to 5 seconds for the action server to come up
	server_up=self.move_base.wait_for_server()
	if not server_up:

	        rospy.logerr("Action server not available!")

	        rospy.signal_shutdown("Action server not available!")
            
		return
	rospy.loginfo("Connected to move_base server")
	

    def robot_goto(self, pos, quat):

        # Send a goal
        self.goal_sent = True
	goal = MoveBaseGoal()
	goal.target_pose.header.frame_id = 'map'
	goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose = Pose(Point(pos['x'], pos['y'], 0.000),
                                     Quaternion(*(quaternion_from_euler(0, 0, goal_orientation*math.pi/180, axes='sxyz'))))

	# Start moving
        self.move_base.send_goal(goal)

	# Allow TurtleBot up to 60 seconds to complete task
	success = self.move_base.wait_for_result(rospy.Duration(60)) 

        state = self.move_base.get_state()
        result = False

        if success and state == GoalStatus.SUCCEEDED:
            # We made it!
            result = True
        else:
            self.move_base.cancel_goal()

        self.goal_sent = False
        return result

    def shutdown(self):
        if self.goal_sent:
            self.move_base.cancel_goal()
        rospy.loginfo("Stop")
        rospy.sleep(1)

if __name__ == '__main__':
    try:

        navigator = tag_navigation()
		
	rospy.loginfo("|----------------------------------|")
	rospy.loginfo("|Enter Goal Position X Coordinate: |")
	rospy.loginfo("|----------------------------------|")
	goal_positionx = input()
	

	rospy.loginfo("|----------------------------------|")
	rospy.loginfo("|Enter Goal Position Y Coordinate: |")
	rospy.loginfo("|----------------------------------|")
	goal_positiony = input()
	
	rospy.loginfo("|--------------------------------|")
	rospy.loginfo("|Enter Orientation:              |")
	rospy.loginfo("|--------------------------------|")
	goal_orientation = input()


        # Customize the following values so they are appropriate for your location
        position = {'x': goal_positionx, 'y' : goal_positiony}
        #quaternion = {'r1' : 0.000, 'r2' : 0.000, 'r3' : 0.000, 'r4' : goal_orientation}
	#quaternion = Quaternion(*(quaternion_from_euler(0, 0, goal_orientation*math.pi/180, axes='sxyz')))

        rospy.loginfo("Go to (%s, %s) pose", position['x'], position['y'])
        success = navigator.robot_goto(position, goal_orientation)

        if success:
            rospy.loginfo("Successfully reached the desired pose")
        else:
            rospy.loginfo("Failed to reach the desired pose")

        # Sleep to give the last log messages time to be sent
        rospy.sleep(1)

    except rospy.ROSInterruptException:
        rospy.loginfo("Ctrl-C Process Interrupted")


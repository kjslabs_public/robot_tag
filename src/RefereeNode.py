#!/usr/bin/env python
# ==============================================================================
# ECE 491
# Robot Tag - RefereeNode.py
# Authors: Kevin Smith (smit2958@msu.edu),
#          Scott Knaub (knaubsco@msu.edu),
#          Sam Campbell (campb980@msu.edu),
#          Alisha Jacob (jacobali@msu.edu)
#
# This file is contains the referee node. It creates and instance of the referee
# class and configures it for a game of robot tag.
# ==============================================================================

# ===[ IMPORTS ]================================================================
import rospy
from robot_tag.CommonSettings import _DEBUG_LEVEL
from robot_tag.Referee import Referee


# ===[ MAIN ]===================================================================
if __name__ == '__main__':
    try:
        rospy.init_node('RobotTag_Referee', anonymous=True, log_level=_DEBUG_LEVEL)

        # Declare the class instance and start the Referee node.
        Referee = Referee()
        Referee.init()

        rospy.spin()
    except rospy.ROSInterruptException:
        pass

# ===[ EOF ]====================================================================

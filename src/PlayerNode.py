#!/usr/bin/env python
# ==============================================================================
# ECE 491
# Robot Tag - PlayerNode.py
# Authors: Kevin Smith (smit2958@msu.edu),
#          Scott Knaub (knaubsco@msu.edu),
#          Sam Campbell (campb980@msu.edu),
#          Alisha Jacob (jacobali@msu.edu)
#
# This file is contains the player node. It creates and instance of the player
# class and configures it for a game of robot tag. 
# ==============================================================================

# ===[ IMPORTS ]================================================================
import rospy
import sys
from robot_tag.Player import Player
from robot_tag.CommonSettings import _DEBUG_LEVEL

# ===[ MAIN ]===================================================================
if __name__ == '__main__':
    try:
        # If the argument is not supplied to the script, then just assume 0
        if len(sys.argv) == 1:
            NodeId = 0
        else:
            NodeId = sys.argv[1]
        rospy.init_node('RobotTag_Player' + str(NodeId), anonymous=True, log_level=_DEBUG_LEVEL)

        # Declare the class instance and start the Referee node.
        Player = Player(NodeId)
        Player.init()

        rospy.spin()
    except rospy.ROSInterruptException:
        pass

# ===[ EOF ]====================================================================

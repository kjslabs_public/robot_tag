#!/usr/bin/env python

import rospy

from robot_tag.RobotTag import RobotTag
from robot_tag.Player import Player
from robot_tag.Referee import Referee

if __name__ == '__main__':
    try:
        rospy.init_node('Robot_Tag_Virtual_Game', anonymous=True)

        # Create virtual Game
        VirtualGame = RobotTag()
        VirtualGame.setupVirtualGame()

        rospy.spin()
    except rospy.ROSInterruptException:
        pass
